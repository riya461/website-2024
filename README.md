# SMC new website

Currently available at [https://beta.smc.org.in](https://beta.smc.org.in).

## Development

Initial setup:

```bash
cd smc/themes/basic
npm install
```

For continuous development, run this in one terminal:

```bash
cd smc/themes/basic && npm run watch-tw
```

and this in another:

```bash
hugo server -D
```

and go to [http://localhost:1313](http://localhost:1313)
