/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["content/**/*.md", "layouts/**/*.html","../../layouts/**/*.html"],
  theme: {
    fontFamily: {
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
}