const setupAController = (name, value, min, max, handler) => {
    const controller = document.createElement('div');
    controller.innerHTML = `${name} <input type="range" value="${value}" min="${min}" max="${max}"/>`
    controller.querySelector('input').addEventListener('change', handler)
    return controller;
}

const getDefault = (prop, preview) => {
    return preview.style[prop] || window.getComputedStyle(preview, prop)
}

const setupControllers = (target, preview) => {
    [
        setupAController("Font Size", getDefault('font-size', preview), '0', '196', (e) => {
            preview.style.fontSize = `${e.target.value}px`;
        }),
        setupAController("Line Height", getDefault('line-height', preview), '0', '500', (e) => {
            preview.style.lineHeight = `${e.target.value}%`
        })
    ].forEach(c => target.appendChild(c))
}

const renderFontExplorer = (explorerNode) => {
    const controllers = document.createElement('div');
    controllers.className = "controllers"
    const preview = document.createElement('div');
    preview.className = "preview"
    explorerNode.appendChild(controllers);
    explorerNode.appendChild(preview);
    setupControllers(controllers, preview)
    preview.innerHTML = `<h2>${explorerNode.dataset.name}</h2><p>This is some preview text</p>`;
};
document.querySelectorAll('.font-explorer').forEach(f => {
    renderFontExplorer(f);
})