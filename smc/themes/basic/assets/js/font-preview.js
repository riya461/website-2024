const previewText = document.getElementById("preview-text");
const fontSizeInput = document.getElementById("font-size-input");
const fontWeightInput = document.getElementById("font-weight-input");

const updatePreview = () => {
  document.querySelectorAll("input[type=range]").forEach(elem => {
    elem.parentElement.querySelector(".range-value").innerText = elem.value
  })

  previewText.style.fontSize = fontSizeInput.value + "px";
  previewText.style.fontWeight = fontWeightInput.value;

  let props = [];
  document.querySelectorAll(".font-variable").forEach(variableInput => {
    props.push(`"${variableInput.getAttribute("propName")}" ${variableInput.value}`)
  })
  previewText.style.fontVariationSettings = props.join(",");
}

updatePreview();
fontSizeInput.addEventListener("input", updatePreview)
fontWeightInput.addEventListener("input", updatePreview)
document.querySelectorAll(".font-variable").forEach(elem => elem.addEventListener("input", updatePreview))
