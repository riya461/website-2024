+++
name = 'Malini'
weight = 10
author = 'Santhosh Thottingal'
fontWeights = [100, 400, 700]
variableFont = true
license = '[SIL Open Font License](https://scripts.sil.org/OFL), Version 1.1.'
repoUrl = 'https://gitlab.com/smc/fonts/malini'

[variables]
  [variables.slant]
    label = "Slant"
    propName = "slnt"
    min = -15.0
    max = 0
    step = 0.1
    default = 0.0
  [variables.width]
    label = "Width"
    propName = "wdth"
    min = 75.0
    max = 125.0
    step = 0.1
    default = 100.0
+++
