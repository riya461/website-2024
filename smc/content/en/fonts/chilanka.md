+++
name = 'Chilanka'
variants = ['Regular']
fontWeights = [400]
weight = 0
author = 'Santhosh Thottingal'
license = '[SIL Open Font License](https://scripts.sil.org/OFL), Version 1.1.'
repoUrl = 'https://gitlab.com/smc/fonts/manjari'
availableOnGoogleFonts = true
+++

## History

Released on October 27, 2014. [Announcement](https://blog.smc.org.in/new-handwriting-style-font-for-malayalam-chilanka)
