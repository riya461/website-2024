+++
name = 'Manjari'
weight = 10
fontWeights = [100, 400, 700]
author = 'Santhosh Thottingal'
license = '[SIL Open Font License](https://scripts.sil.org/OFL), Version 1.1.'
repoUrl = 'https://gitlab.com/smc/fonts/manjari'
availableOnGoogleFonts = true
+++

## Credits

Manjari belongs to the typefaces released by Swathanthra Malayalam Computing, a volunteer driven organization for developing and promoting free and opensource software for Malayalam computing. It shares the experience and expertise by the organization in its more than a decade effort in standardizing and improving opentype Malayalam typeface technology. Manjari was released on July 23, 2016.

Designer: Santhosh Thottingal, Opentype Engineering: Santhosh Thottingal and Kavya Manohar

Manjari inherits the traditional orthography style opentype font development practices pioneered by Rachana by K H Hussain and Rachana Aksharavedi. This includes glyph naming conventions, the supported ligature collection, a significant parts of opentype rules.

Acknowledgements(in alphabetical order)

- K H Hussain, Designer of Rachana font.
- Raph Levien, Author of Spiro library.
- Rajeesh K V, Opentype engineering.
- Swathanthra Malayalam computing and its volunteers for all help and support.

## FAQ

### Can I help with improving Manjari?

Yes you can! Manjari is an open-source project, meaning the source code—or "source design" if you will—that is used to build the font files are freely available to improve upon. Font making requires a fair bit of technical work and depending on what you'd like to do, some things might be more fun depending on your technical skills. Please start by reading and exploring the source code and design files provided.

### Can I use Manjari in google docs?

Yes you can. Please see the detailed instructions at [SMC blog](https://blog.smc.org.in/manjari-gayathri-chilanka-in-google-fonts)
