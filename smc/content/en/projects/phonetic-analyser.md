+++
title = 'Malayalam phonetic analyser and generator'
draft = false
val = 'Malayalam Linguistics'
img = 'none'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
details = 'https://phon.smc.org.in/'
source = 'https://gitlab.com/smc/mlphon'
+++
Malayalam Phonetic analyser and generator, IPA convertor. 