+++
title = 'indicngram'
draft = false
val = 'Programming Libraries'
img = 'none'
source = 'https://github.com/libindic/indicngram'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
details = 'none'
smmary="n-gram genereator for indic languages"
+++

An n-gram model is a type of probabilistic model for predicting the next item in a sequence. n-grams are used in various areas of statistical natural language processing and genetic sequence analysis. An n-gram is a subsequence of n items from a given sequence. The items in question can be phonemes, syllables, letters, words or base pairs according to the application. An n-gram of size 1 is referred to as a "unigram"; size 2 is a "bigram" (or, less commonly, a "digram"); size 3 is a "trigram"; and size 4 or more is simply called an "n-gram". 