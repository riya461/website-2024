+++
title = 'Speech corpus'
draft = false
val = 'Corpus and data collections'
img = 'none'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
details = 'http://msc.smc.org.in/'
source = 'http://gitlab.com/smc/msc'
+++
A collection of Malayalam speech samples for various research purpose, curated and released in Creative commons license. 