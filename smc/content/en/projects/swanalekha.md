+++
title = 'Swanalekha'
img = '/images/ml-swanalekha.png'
draft = false
val = 'Input tools'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
details = 'https://swanalekha.smc.org.in/'
source = 'https://gitlab.com/smc/swanalekha'
+++

Swanalekha is a famous transliteration based input tool available in all operating systems in desktops and mobile phones. It is very easy to learn using familiar Manglish based key mappings.