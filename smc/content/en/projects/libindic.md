+++
title = 'Libindic'
draft = false
val = 'Web'
img = 'none'
source = 'none'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
details = 'https://libindic.org/'
+++

Libindic is a web platform to host Free Software language processing applications. It consists of a web framework and a set of applications for processing Indian languages. It is a platform for porting existing and upcoming language processing applications to the web. 