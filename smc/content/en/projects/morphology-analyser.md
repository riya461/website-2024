+++
title = 'Morphology analyser and generator'
draft = false
val = 'Malayalam Linguistics'
img = 'none'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
details = 'https://morph.smc.org.in/'
source = 'https://gitlab.com/smc/mlmorph/'
+++
Morphology analyser and generator, a foundational component for Malayalam computational linguistics. 