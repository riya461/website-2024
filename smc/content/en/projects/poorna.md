+++
title = 'Poorna'
img = '/images/poorna-logo.png'
draft = false
val = 'Input tools'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
details = 'https://poorna.smc.org.in'
source = 'https://gitlab.com/smc/poorna/'
+++
Poorna is an extended Inscript/Remington keyboard layout available for Windows, Linux and Mac. It includes all Malayalam unicode characters which are not found in other layouts. One important use case is typesetting an old book (Malayalam bible for example) that requires characters not present in the other layouts. This layout also makes it easy to type punctuations and other special characters without switching layouts 