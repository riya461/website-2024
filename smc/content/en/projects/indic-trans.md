+++
title = 'indic-trans'
draft = false
val = 'Programming Libraries'
img = 'none'
source = 'https://github.com/libindic/indic-trans'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='https://indic-trans.readthedocs.io/en/latest/'
details = 'https://irshadbhat.github.io/gsoc/'
summary = "cross transliterations among all Indian languages"
+++


The project aims on adding a state-of-the-art transliteration module for cross transliterations among all Indian languages including English and Urdu. The module currently supports the following languages: Hindi, Bengali, Gujarati, Punjabi, Malayalam, Kannada, Tamil, Telugu, Oriya, Marathi, Assamese, Konkani, Bodo, Nepali, Urdu and English 