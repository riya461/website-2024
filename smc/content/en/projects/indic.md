+++
title = 'Indic Keyboard'
img = '/images/indickeyboard.png'
draft = false
val = 'Input tools'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
details = 'https://indic.app/'
source = 'https://gitlab.com/indicproject/indic-keyboard'
+++
Indic Keyboard is a MOSS Award winning, privacy aware versatile keyboard for Android users who wish to use Indic and Indian languages to type messages, compose emails and generally prefer to use them in addition to English on their phone. You can use this application to type anywhere in your phone that you would normally type in English. It currently supports 23 languages and 57 layouts. 