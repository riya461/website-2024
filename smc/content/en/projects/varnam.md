+++
title = 'Varnam'
img = '/images/varnam.gif'
draft = false
val = 'Input tools'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
details = 'https://www.varnamproject.com/'
source = 'https://github.com/varnamproject/'
+++
Varnam is a cross platform predictive transliterator for Indic languages. Varnam aims at providing consistent input experience across different platforms in almost all indic languages. 