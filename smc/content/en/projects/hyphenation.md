+++
title = 'Hyphenation'
draft = false
val = 'Programming Libraries'
img = 'none'
source = 'https://github.com/libindic/hyphenation'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
details = 'none'
summary="Hyphenate Text"
+++


Hyphenation is the process inserting hyphens in between the syllables of a word so that when the text is justified, maximum space is utilized. Supported Languages: English, Hindi, Malayalam, Tamil, Telugu, Kannada, Oriya, Bengali, Gujarati, Panjabi, Marathi, Sanskrit, Assamese, Kashmeeri, Afrikaans, German, French, Croatian, Hungarian, Italian, Zulu 