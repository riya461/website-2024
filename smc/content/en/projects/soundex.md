+++
title = 'Soundex'
draft = false
val = 'Programming Libraries'
img = 'none'
source = 'https://github.com/libindic/soundex'
use = 'none'
tools = 'none'
firefox = 'none'
chrome = 'none'
playstore='none'
fdroid='none'
howto='none'
download='none'
doc='none'
details = 'https://thottingal.in/blog/2009/07/26/indicsoundex/'

summary="Soundex Phonetic Code Algorithm Demo for Indian Languages. "
+++

Soundex is phonetic algorithm for indexing names by sound as pronounced in English. LibIndic's soundex module implements Soundex algorithm for Engish as well as a modified version of soundex algorithm for Indian languages. 