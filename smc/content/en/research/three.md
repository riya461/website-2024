+++
title = 'Malayalam Orthographic Reforms: Impact on Language and Popular Culture '
draft = false
val = 'Published Papers'
mlphon = 'none'
code = 'none'
blog = 'none'
read='none'
analyse = 'none'
author = 'Santhosh Thottingal, Kavya Manohar'
link = 'https://thottingal.in/documents/Malayalam%20Orthographic%20Reforms_%20Impact%20on%20Language%20and%20Popular%20Culture.pdf'
+++

Proceedings of <a href = "http://conferences.telecom-bretagne.eu/grafematik/">[grafematik](http://conferences.telecom-bretagne.eu/grafematik/)</a> : Graphemics in the 21st century—From graphemes to knowledge, Pôle numérique Brest Iroise at Brest, France, on June 14-15, 2018.