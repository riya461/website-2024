+++
title = 'Information, Entropy and Malayalam '
draft = false
val = 'Malayalam Computational Linguistics'
mlphon = 'none'
code = 'none'
blog = 'none'
analyse = 'none'
author = ' Kavya Manohar'
read= 'https://kavyamanohar.com/post/malayalam-ipa-consonants/'
link = 'none'
+++

What is the information Entropy of Malayalam language? How to calculate it? Published on July 18, 2019
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;