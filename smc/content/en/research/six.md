+++
title = 'Mlphon: A Multifunctional Grapheme-Phoneme Conversion Tool Using Finite State Transducers'
draft = false
code = 'none'
blog = 'none'

read='none'
analyse = 'none'
val = 'Published Papers'
mlphon = 'https://phon.smc.org.in/'
author = 'Kavya Manohar, A. R. Jayan, Rajeev Rajan'
link = 'https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=9877808'
+++

Mlphon computationally models linguistic rules using finite state transducers and performs multiple functions including grapheme to phoneme (g2p) and phoneme to grapheme (p2g) conversions, syllabification, phonetic feature analysis and script grammar check.