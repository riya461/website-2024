+++
title = 'അക്ഷരങ്ങളുടെ ഡിജിറ്റൽ കാലം - അച്ചടിയിൽ നിന്ന് ഡിജിറ്റൽ ഫോണ്ടുകളിലെത്തിയ മലയാളത്തിന്റെ വർത്തമാനം'
draft = false
code = 'none'
blog = 'none'

read='none'
analyse = 'none'
val = 'Published Papers'
mlphon = 'none'
author = 'Santhosh Thottingal'
link = 'https://santhoshtr.gitlab.io/digitaltypography-article/digitaltypography.pdf'
+++

അച്ചടിയുടെ ഇരുന്നൂറാം വാർഷികത്തിൽ ബെഞ്ചമിൻ ബെയിലി ഫൗണ്ടേഷൻ പ്രസിദ്ധീകരിച്ച പ്രബന്ധം 