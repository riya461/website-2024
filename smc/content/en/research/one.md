+++
title = 'Quantitative Analysis of Morphological Complexity of Malayalam Language'
draft = false
code = 'none'
blog = 'none'
mlphon = 'none'

read='none'
analyse = 'none'
val = 'Published Papers'
author = 'Kavya Manohar, A. R. Jayan, Rajeev Rajan'
link = 'https://kavyamanohar.com/documents/tsd_morph_complexity_ml.pdf'
+++
Proceedings of the 23rd International Conference on Text, Speech and Dialogue September 8-11, 2020, Brno, Czech Republic. 